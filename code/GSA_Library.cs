﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using EllieMae.Encompass.Client;
using EllieMae.Encompass.BusinessObjects;
using EllieMae.Encompass.BusinessObjects.Loans;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading;
using Word = Microsoft.Office.Interop.Word;
using System.Runtime.InteropServices;
using EllieMae.Encompass.Collections;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.ComponentModel;
using EllieMae.Encompass.BusinessObjects.Loans.Logging;
using Microsoft.Office.Interop.Word;
using System.Collections;

namespace GSA_Library
{
    [Plugin]
    public class GSA_Plugin
    {
        public GSA_Plugin()
        {
            EncompassApplication.LoanOpened += EncompassApplication_LoanOpened;
        }

        private void EncompassApplication_LoanOpened(object sender, EventArgs e)
        {
            EncompassApplication.CurrentLoan.FieldChange += CurrentLoan_FieldChange;
        }

        private void CurrentLoan_FieldChange(object source, FieldChangeEventArgs e)
        {
            if (e.FieldID == "CX.SERVICES" && e.NewValue == "GSA_Plugin")
            {
                GSA_Code.GSA_Run(EncompassApplication.CurrentLoan);
            }
        }
    }

    public class GSA_Code
    {
        public static JsonSerializerSettings JSONResponseType { get; private set; }
        public static void GSA_Run(Loan loan)
        {
            string path = System.IO.Path.GetTempPath();
            string path1 = System.IO.Path.GetTempPath();
            path = Path.Combine(path, "GSA_Results.doc");
            path1 = Path.Combine(path1, "GSA_Results1.doc");
            List<string> list = new List<string>();
            List<string> list1 = new List<string>();
            List<string> list2 = new List<string>();
            DateTime localdate = DateTime.Now;

            string loannum = "Loan Number: " + loan.LoanNumber;
            string time = "                                                                                                                   Date: " + localdate.ToString();
            string bwrName = "";
            string cobwrName = "";
            if (!string.IsNullOrEmpty(loan.Fields["4000"].FormattedValue))
            {
                bwrName = loan.Fields["4000"].FormattedValue + " " + loan.Fields["4001"].FormattedValue + " " + loan.Fields["4002"].FormattedValue; //creating variables for every required loan field
            }
            if (!string.IsNullOrEmpty(loan.Fields["4004"].FormattedValue))
            {
                cobwrName = loan.Fields["4004"].FormattedValue + " " + loan.Fields["4005"].FormattedValue + " " + loan.Fields["4006"].FormattedValue;
            }
            string escrowcom = loan.Fields["610"].FormattedValue;
            string titlecom = loan.Fields["411"].FormattedValue;
            string mb = loan.Fields["LoanTeamMember.Name.Mortgage Banker"].FormattedValue;
            string escrowoff = loan.Fields["611"].FormattedValue;
            string titleoff = loan.Fields["416"].FormattedValue;
            string appraise = loan.Fields["618"].FormattedValue;
            string apprcom = loan.Fields["617"].FormattedValue;
            string process = loan.Fields["LoanTeamMember.Name.Loan Processor"].FormattedValue;
            string under = loan.Fields["984"].FormattedValue;
            string akaborrow = loan.Fields["1869"].FormattedValue;
            string akacoborrow = loan.Fields["1874"].FormattedValue;
            string[] parsed = akaborrow.Split('\r', '\n', ';');
            string[] parsed1 = akacoborrow.Split(';');
            try
            {
                if (!string.IsNullOrEmpty(appraise))
                {
                    list.Add(appraise.TrimStart(' '));
                    list1.Add(appraise.TrimStart(' '));
                    list2.Add("Appraiser");
                }
                else
                {
                    list1.Add(appraise.TrimStart(' '));
                    list2.Add("Appraiser");
                }
                if (!string.IsNullOrEmpty(apprcom))
                {
                    list.Add(apprcom.TrimStart(' '));
                    list1.Add(apprcom.TrimStart(' '));
                    list2.Add("Appraisal Company");
                }
                else
                {
                    list1.Add(apprcom.TrimStart(' '));
                    list2.Add("Appraisal Company");
                }
                list.Add(bwrName.TrimStart(' ')); //adding each field varaible to the list that will be printed
                list1.Add(bwrName.TrimStart(' '));
                list2.Add("Borrower 1");

                if (!string.IsNullOrEmpty(cobwrName))
                {
                    list.Add(cobwrName.TrimStart(' '));
                    list1.Add(cobwrName.TrimStart(' '));
                    list2.Add("Borrower 2");
                }
                if (loan.Fields["19"].FormattedValue == "Purchase")
                {
                    string buyerag = loan.Fields["VEND.X139"].FormattedValue;
                    if (!string.IsNullOrEmpty(buyerag))
                    {
                        list.Add(buyerag.TrimStart(' '));
                        list1.Add(buyerag.TrimStart(' '));
                        list2.Add("Buyer's Agent");
                    }
                    else
                    {
                        list1.Add(buyerag.TrimStart(' '));
                        list2.Add("Buyer's Agent");
                    }
                }
                if (loan.BorrowerPairs.Count > 1)
                {
                    for (int s = 1; s < loan.BorrowerPairs.Count; s++)
                    {
                        BorrowerPair pairA = loan.BorrowerPairs[s];
                        loan.BorrowerPairs.Current = pairA;
                        string comortg = loan.Fields["4000"].FormattedValue + " " + loan.Fields["4001"].FormattedValue + " " + loan.Fields["4002"].FormattedValue;
                        if (!string.IsNullOrEmpty(comortg))
                        {
                            list.Add(comortg.TrimStart(' '));
                            list1.Add(comortg.TrimStart(' '));
                            list2.Add("Co-Mortgagor " + s);
                        }
                    }
                    loan.BorrowerPairs.Current = loan.BorrowerPairs[0];
                }
                if (!string.IsNullOrEmpty(escrowcom))
                {
                    list.Add(escrowcom.TrimStart(' '));
                    list1.Add(escrowcom.TrimStart(' '));
                    list2.Add("Escrow Company");
                }
                else
                {
                    list1.Add(escrowcom.TrimStart(' '));
                    list2.Add("Escrow Company");
                }
                if (!string.IsNullOrEmpty(escrowoff))
                {
                    list.Add(escrowoff.TrimStart(' '));
                    list1.Add(escrowoff.TrimStart(' '));
                    list2.Add("Escrow Officer");
                }
                else
                {
                    list1.Add(escrowoff.TrimStart(' '));
                    list2.Add("Escrow Officer");
                }

                if (!string.IsNullOrEmpty(process))
                {
                    list.Add(process.TrimStart(' '));
                    list1.Add(process.TrimStart(' '));
                    list2.Add("Loan Processor");
                }
                else
                {
                    list1.Add(process.TrimStart(' '));
                    list2.Add("Loan Processor");
                }

                if (!string.IsNullOrEmpty(mb))
                {
                    list.Add(mb.TrimStart(' '));
                    list1.Add(mb.TrimStart(' '));
                    list2.Add("Mortgage Banker");
                }
                else
                {
                    list1.Add(mb.TrimStart(' '));
                    list2.Add("Mortgage Banker");
                }

                if (loan.Fields["19"].FormattedValue == "Purchase")
                {  // if the file is a purchase the following will be created and added to the list
                    string seller = loan.Fields["638"].FormattedValue;
                    if (!string.IsNullOrEmpty(seller))
                    {
                        list.Add(seller.TrimStart(' '));
                        list1.Add(seller.TrimStart(' '));
                        list2.Add("Seller 1");
                    }
                    else
                    {
                        list1.Add(seller.TrimStart(' '));
                        list2.Add("Seller 1");
                    }
                }
                if (loan.Fields["19"].FormattedValue == "Purchase")
                {  // if the file is a purchase the following will be created and added to the list
                    string seller = loan.Fields["638"].FormattedValue;
                    string seller1 = loan.Fields["VEND.X412"].FormattedValue;
                    if (!string.IsNullOrEmpty(seller1))
                    {
                        list.Add(seller1.TrimStart(' '));
                        list1.Add(seller1.TrimStart(' '));
                        list2.Add("Seller 2");
                    }
                    else
                    {
                        list1.Add(seller1.TrimStart(' '));
                        list2.Add("Seller 2");
                    }
                    string sellerag = loan.Fields["VEND.X150"].FormattedValue;
                    if (!string.IsNullOrEmpty(sellerag))
                    {
                        list.Add(sellerag.TrimStart(' '));
                        list1.Add(sellerag.TrimStart(' '));
                        list2.Add("Seller's Agent");
                    }
                    else
                    {
                        list1.Add(sellerag.TrimStart(' '));
                        list2.Add("Seller's Agent");
                    }
                }

                if (!string.IsNullOrEmpty(titleoff))
                {
                    list.Add(titleoff.TrimStart(' '));
                    list1.Add(titleoff.TrimStart(' '));
                    list2.Add("Title Officer");
                }
                else
                {
                    list1.Add(titleoff.TrimStart(' '));
                    list2.Add("Title Officer");
                }
                if (!string.IsNullOrEmpty(titlecom))
                {
                    list.Add(titlecom.TrimStart(' '));
                    list1.Add(titlecom.TrimStart(' '));
                    list2.Add("Title Company");
                }
                else
                {
                    list1.Add(titlecom.TrimStart(' '));
                    list2.Add("Title Company");
                }

                if (!string.IsNullOrEmpty(under))
                {
                    list.Add(under.TrimStart(' '));
                    list1.Add(under.TrimStart(' '));
                    list2.Add("Underwriter");
                }
                else
                {
                    list1.Add(under.TrimStart(' '));
                    list2.Add("Underwriter");
                }

                foreach (string s in parsed)
                {
                    if (!string.IsNullOrEmpty(s) && !string.IsNullOrEmpty(bwrName))
                    {
                        list.Add(s.TrimStart(' '));
                        list1.Add(s.TrimStart(' '));
                        list2.Add("Borrower 1 AKA");
                    }
                }
                foreach (string s1 in parsed1)
                {
                    if (!string.IsNullOrEmpty(s1) && !string.IsNullOrEmpty(cobwrName))
                    {
                        list.Add(s1.TrimStart(' '));
                        list1.Add(s1.TrimStart(' '));
                        list2.Add("Borrower 2 AKA");
                    }
                }
            }
            catch { }
            string[,,] tableout = new string[list1.Count, 3, 3];
            StringBuilder sb = new StringBuilder(); //objects to print
            StringBuilder st = new StringBuilder(); //second one to not overuse one object while printing to two locations
            int count = 0;
            int i = 0;
            int y = 0;
            foreach (string keys in list1) //prints the name of searched terms
            {
                if (keys == "")
                {
                    if (i % 5 == 0 && i >= 4)
                    {
                        Thread.Sleep(5000);
                    }
                    i++;
                    continue;
                }
                try
                {
                    if (i % 5 == 0)
                    {
                        Thread.Sleep(5000);
                    }

                    HttpWebRequest request = WebRequest.Create(" https://api.data.gov/sam/v1/registrations?qterms=" + keys + "&start=1&length=500&api_key=Ykxpa5dBM1Uly73bK6Unmb1Q7Jd3mi2MHp3TeAcQ") as HttpWebRequest;
                    //Creating a web request to access the API   
                    request.Method = "GET";

                    request.ContentType = "application/json";

                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {

                        if (response.StatusCode != HttpStatusCode.OK) throw new Exception(String.Format("Server error (HTTP {0}: {1}).", response.StatusCode, response.StatusDescription));
                        Stream stream1 = response.GetResponseStream(); //converting response into a stream
                        StreamReader sr = new StreamReader(stream1);
                        string strsb = sr.ReadToEnd(); //converting stream into string type
                        object objResponse = JsonConvert.DeserializeObject(strsb, JSONResponseType);
                        string result = Convert.ToString(objResponse);
                        JObject output = JObject.Parse(result);
                        if (y == 0)
                        {
                            sb.AppendFormat("{0,87}", "GSA SAM Search Results");
                            sb.AppendLine();
                            sb.AppendFormat("{0,90}", "List of records matching your search for:\v");
                            sb.AppendFormat("{0,0}", "Title:");
                            sb.AppendFormat("{0,65}", "Search Terms:");
                            sb.AppendFormat("{0, 56}", "Search Results:\v");
                            sb.AppendLine();
                            y++;
                        }
                        if (!output["results"].HasValues) // Only executes if the results return empty
                        {

                            tableout[i, 1, 0] = "No Results Found";
                        }
                        else
                        {
                            tableout[i, 1, 0] = "Please See Attached For Results";
                            tableout[i, 1, 1] = keys;
                            count++;

                            foreach (var item1 in output["results"]) //shuffling through every query that resulted in a response executing the following code for each
                            {                                          // formatting and accessing the item object
                                if (item1["status"].ToString() == "Active") //printing the results if a query gets a response
                                {
                                    st.AppendFormat("{0, 10}", "Status: ").Append(item1["status"]);
                                    st.AppendFormat("{0, 110}", "Name: " + item1["legalBusinessName"]);
                                    st.AppendLine();
                                    st.AppendFormat("{0, 0}", "DUNS: " + item1["duns"]);
                                    st.AppendFormat("{0, 130}", "CAGE Code: ").Append(item1["cage"]);
                                    st.AppendLine();
                                    st.Append("Address: " + item1["samAddress"]["line1"]);
                                    st.AppendFormat("{0,105}", "City: ").Append(item1["samAddress"]["city"]);
                                    st.AppendLine();
                                    st.Append("State/Province: " + item1["samAddress"]["stateOrProvince"]);
                                    st.AppendFormat("{0, 60}", "Country: " + item1["samAddress"]["country"]);
                                    st.AppendFormat("{0, 61}", "Zipcode: ").Append(item1["samAddress"]["zip"]);
                                    st.AppendLine();
                                    st.Append('_', 85);
                                }
                                else
                                {
                                    st.AppendFormat("{0, 10}", "Status: ").Append("Inactive");
                                    st.AppendFormat("{0, 110}", "Name: " + item1["legalBusinessName"]);
                                    st.AppendLine();
                                    st.AppendFormat("{0, 0}", "DUNS: " + item1["duns"]);
                                    st.AppendFormat("{0, 130}", "CAGE Code: ").Append(item1["cage"]);
                                    st.AppendLine();
                                    st.Append("Address: " + item1["samAddress"]["line1"]);
                                    st.AppendFormat("{0,105}", "City: ").Append(item1["samAddress"]["city"]);
                                    st.AppendLine();
                                    st.Append("State/Province: " + item1["samAddress"]["stateOrProvince"]);
                                    st.AppendFormat("{0, 60}", "Country: " + item1["samAddress"]["country"]);
                                    st.AppendFormat("{0, 61}", "Zipcode: ").Append(item1["samAddress"]["zip"]);
                                    st.AppendLine();
                                    st.Append('_', 85);
                                }
                            }
                        }
                        i++;
                    }
                }
                catch (Exception e) //catches exceptions
                {
                    File.WriteAllText("C:\\Test Err\\Loan_Fields_Err.txt", e.Message);
                }

                //Attachment att = loan.Attachments.Add("C:\\Test PDF\\GSA_Results.DOC");
                //Attachment att1 = loan.Attachments.Add("C:\\Test PDF\\GSA_Results1.DOC");
                //LogEntryList gsa = loan.Log.TrackedDocuments.GetDocumentsByTitle("FHA-GSA/LDP Search");
                //LogEntryList gsa1 = loan.Log.TrackedDocuments.GetDocumentsByTitle("FHA-GSA/LDP Search");
            }
            //if (list.Count > 0)
            //{
            //    sb.Append("The following fields are missing from the loan: ");
            //    sb.AppendLine();
            //    for(int y =0;  y < list.Count; y++)
            //    {
            //        sb.Append(list[y] + "  ");
            //    }
            //}
            try
            {
                File.WriteAllText(path, sb.ToString());
                File.WriteAllText(path1, st.ToString());
            }
            catch
            { }

            for (int b = 0; b < list1.Count; b++)
            {
                if (!string.IsNullOrEmpty(list1[b]))
                {
                    tableout[b, 0, 0] = list1[b];
                    tableout[b, 0, 1] = list2[b];
                }
                else
                {
                    tableout[b, 0, 0] = "Missing Name";
                    tableout[b, 1, 0] = "Incomplete";
                    tableout[b, 0, 1] = list2[b];
                }
            }

            Word.Application application = null;  //reading and saving the txt document to a .doc
            object oMissing = System.Reflection.Missing.Value;
            object oEndOfDoc = "\\endofdoc"; //endofdoc is a predefined bookmark 

            //Start Word and create a new document.
            Word._Application oWord;
            Word._Document oDoc;
            oWord = new Word.Application();
            oWord.Visible = false;
            oDoc = oWord.Documents.Add(ref oMissing, ref oMissing, ref oMissing, ref oMissing);
            try
            {
                application = new Word.Application();
                var document = application.Documents.Add();
                var paragraph = document.Paragraphs.Add();
                document.ReadOnlyRecommended = true;
                paragraph.Range.Font.Bold = 1;
                paragraph.Range.Text = File.ReadAllText(path);
                foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
                {
                    Microsoft.Office.Interop.Word.Range headerRange = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                    headerRange.Fields.Add(headerRange, Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage);
                    headerRange.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;
                    headerRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdGray50;
                    headerRange.Font.Size = 8;
                    headerRange.Font.Bold = 1;
                    headerRange.Font.Name = "Arial";
                    headerRange.Text = loannum + time;
                }

                Word.Table oTable;
                Word.Range wrdRng = document.Bookmarks.get_Item(ref oEndOfDoc).Range;
                oTable = document.Tables.Add(wrdRng, list1.Count, 3, ref oMissing, ref oMissing);
                oTable.Range.ParagraphFormat.SpaceAfter = 1;
                int r = 1, c = 1;
                string strText;
                for (int e = 0; e < list1.Count; e++) //prints the name of searched terms
                {
                    strText = tableout[e, 0, 1];
                    oTable.Cell(r, c).Range.Text = strText;
                    if (string.IsNullOrEmpty(list1[e]))
                    {
                        oTable.Cell(r, c).Range.Font.Color = WdColor.wdColorRed;
                        oTable.Cell(r, c).Range.Font.Bold = 1;
                    }
                    oTable.Cell(r, c).Range.Borders[WdBorderType.wdBorderBottom].LineStyle = WdLineStyle.wdLineStyleSingle;
                    oTable.Cell(r, c).Range.Borders[WdBorderType.wdBorderTop].LineStyle = WdLineStyle.wdLineStyleSingle;
                    r++;
                }
                r = 1;
                for (int f = 0; f < list1.Count; f++) //prints the name of searched terms
                {
                    c = 2;
                    strText = tableout[f, 0, 0];
                    oTable.Cell(r, c).Range.Text = strText;
                    if (string.IsNullOrEmpty(list1[f]))
                    {
                        oTable.Cell(r, c).Range.Font.Color = WdColor.wdColorRed;
                        oTable.Cell(r, c).Range.Font.Bold = 1;
                    }
                    oTable.Cell(r, c).Range.Borders[WdBorderType.wdBorderBottom].LineStyle = WdLineStyle.wdLineStyleSingle;
                    oTable.Cell(r, c).Range.Borders[WdBorderType.wdBorderTop].LineStyle = WdLineStyle.wdLineStyleSingle;
                    r++;
                }
                r = 1;
                for (int j = 0; j < list1.Count; j++) //prints the name of searched terms
                {
                    c = 3;
                    strText = tableout[j, 1, 0];
                    oTable.Cell(r, c).Range.Text = strText;
                    if (string.IsNullOrEmpty(list1[j]))
                    {
                        oTable.Cell(r, c).Range.Font.Color = WdColor.wdColorRed;
                        oTable.Cell(r, c).Range.Font.Bold = 1;
                    }
                    oTable.Cell(r, c).Range.Borders[WdBorderType.wdBorderBottom].LineStyle = WdLineStyle.wdLineStyleSingle;
                    oTable.Cell(r, c).Range.Borders[WdBorderType.wdBorderTop].LineStyle = WdLineStyle.wdLineStyleSingle;
                    r++;
                }
                //Printing missing loan fields
                var paragraph1 = document.Paragraphs.Add();
                application.ActiveDocument.SaveAs(path, Word.WdSaveFormat.wdFormatDocument);
                document.Close();
            }
            finally
            {
                if (application != null)
                {
                    application.Quit();
                    Marshal.FinalReleaseComObject(application);
                }
            }

            Word.Application application1 = null;  //reading and saving the txt document to a .doc
            try
            {
                application1 = new Word.Application();
                var document = application1.Documents.Add();
                var paragraph = document.Paragraphs.Add();
                document.ReadOnlyRecommended = true;
                paragraph.Range.Text = File.ReadAllText(path1);
                paragraph.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                document.Paragraphs.SpaceAfter = 0;
                document.Paragraphs.LineSpacing = 11;
                foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
                {
                    Microsoft.Office.Interop.Word.Range headerRange = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                    headerRange.Fields.Add(headerRange, Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage);
                    headerRange.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;
                    headerRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdGray50;
                    headerRange.Font.Size = 8;
                    headerRange.Font.Bold = 1;
                    headerRange.Font.Name = "Arial";
                    headerRange.Text = loannum + time;
                }
                //string filename = GetFullName();
                application1.ActiveDocument.SaveAs(path1, Word.WdSaveFormat.wdFormatDocument, Word.WdSaveOptions.wdDoNotSaveChanges);
                document.Close();
            }
            finally
            {
                if (application1 != null)
                {
                    application1.Quit();
                    Marshal.FinalReleaseComObject(application1);
                }
            }
            try
            {
                Attachment att = loan.Attachments.Add(path);
                Attachment att1 = loan.Attachments.Add(path1);

                LogEntryList gsa_results = loan.Log.TrackedDocuments.GetDocumentsByTitle("FHA - GSA/LDP Search");
                att.Title = "GSA Search Summary: " + DateTime.Now;
                att1.Title = "GSA Search Results: " + DateTime.Now;
                if (gsa_results.Count > 0)
                {
                    TrackedDocument gsa_result = (TrackedDocument)
                    gsa_results[0];
                    gsa_result.Attach(att);
                    gsa_result.Attach(att1);
                }
                else
                {
                    TrackedDocument customDoc = loan.Log.TrackedDocuments.Add("FHA - GSA/LDP Search", "Submittal");
                    customDoc.OrderDate = DateTime.Today;
                    customDoc.Attach(att);
                    customDoc.Attach(att1);
                }
            }
            catch (Exception d)
            {
                File.WriteAllText("C:\\Test Err\\Loan_Fields_Err2.txt", d.Message); ;
            }
            // LogEntryList exist = loan.Log.UnderwritingConditions.GetConditionsByTitle("2204");
            LogUnderwritingConditions logs = loan.Log.UnderwritingConditions;
            bool exists = false;
            foreach (UnderwritingCondition conditions in logs)
            {
                if (conditions.Title == "2204")
                {
                    exists = true;
                    if (count > 0)
                    {
                        conditions.Description = "LDP/GSA Results have been found for the following parties: ";
                        for (int e = 0; e < (i - 1); e++)
                        {
                            if (!string.IsNullOrEmpty(tableout[e, 1, 1]))
                            {
                                conditions.Description += tableout[e, 1, 1] + ", ";
                            }
                        }
                        conditions.Description += tableout[i, 1, 1] + " Please See Attached For Results";
                    }
                    else
                    {
                        conditions.Description = "LDP/GSA All parties have been searched. No Results Found";
                    }
                }
                else
                {
                    continue;
                }
            }
            if (exists == false)
            {
                UnderwritingCondition condition = loan.Log.UnderwritingConditions.Add("2204");
                if (count > 0)
                {
                    condition.Description = "LDP/GSA Results have been found for the following parties: ";
                    for (int e = 0; e < (i - 1); e++)
                    {
                        if (!string.IsNullOrEmpty(tableout[e, 1, 1]))
                        {
                            condition.Description += tableout[e, 1, 1] + ", ";
                        }
                    }
                    condition.Description += tableout[i, 1, 1] + " Please See Attached For Results";
                }
                else
                {
                    condition.Description = "LDP/GSA All parties have been searched. No Results Found";
                }
            }
        }
    }
}
