﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EllieMae.Encompass.Client;
using EllieMae.Encompass.BusinessObjects;
using EllieMae.Encompass.BusinessObjects.Users;
using EllieMae.Encompass.BusinessObjects.Loans;
using EllieMae.Encompass.BusinessObjects.Loans.Logging;
using System.Xml;
using EllieMae.Encompass.Collections;
using System.IO;
using System.Windows;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.ComponentModel;

namespace FileStarter_Btn
{
    [Plugin]
    public class FileStarter_Btn
    {
        public FileStarter_Btn()
        {
            EncompassApplication.LoanOpened += EncompassApplication_LoanOpened;
        }

        private void EncompassApplication_LoanOpened(object sender, EventArgs e)
        {
            EncompassApplication.CurrentLoan.FieldChange += CurrentLoan_FieldChange; ;
        }
        private void CurrentLoan_FieldChange(object source, FieldChangeEventArgs e)
        {
            int count = 0;
            string input = e.NewValue;
            string ninput = "";
            string userID = "";
            string role = "";
            char[] delim = { ';' };
            string[] work = input.Split(delim);
            foreach (string s in work)
            {
                if(count == 0)
                {
                    ninput = s.TrimStart();
                }
                if (count == 1)
                {
                    userID = s.TrimStart();
                }
                if (count == 2)
                {
                    role = s.TrimStart();
                }
                count++;
            }
            if (e.FieldID == "CX.SERVICES" && ninput == "Assign_Role" || ninput == "FileStarter_Btn")
            {
                Program.FileStarter_Run(EncompassApplication.CurrentLoan, userID, role);
            }
        }
    }
    public class Program
    {
        public static object MessageBox { get; private set; }

        public static void FileStarter_Run(Loan loan, string userID, string nrole)
        {
            if (nrole == "")
            {
                // Retrieve the Loan Officer role (LO)
                Role role = loan.Session.Loans.Roles.GetRoleByAbbrev("FS");

                // Retrieve the desired user for the role
                User user = loan.Session.Users.GetUser(userID);

                // Assign the user to the specified role
                loan.Associates.AssignUser(role, user);
            }
            else
            {
                // Retrieve the Loan Officer role (LO)
                Role role = loan.Session.Loans.Roles.GetRoleByAbbrev(nrole);
                if (role == null)
                {
                    //DISPLAY ERR message "Please Enter Valid userID" 
                    System.Windows.Forms.MessageBox.Show("Please Enter Valid Role");
                }
                // Retrieve the desired user for the role
                User user = loan.Session.Users.GetUser(userID);
                if (user == null)
                {
                    //DISPLAY ERR message "Please Enter Valid userID" 
                    System.Windows.Forms.MessageBox.Show("Please Enter Valid userID");
                }
                // Assign the user to the specified role
                else if (user != null && role != null)
                {
                    loan.Associates.AssignUser(role, user);
                }
                }

        }
    }
}
