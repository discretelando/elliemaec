﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EllieMae.Encompass.Client;
using EllieMae.Encompass.BusinessObjects;
using EllieMae.Encompass.BusinessObjects.Loans;
using EllieMae.Encompass.BusinessObjects.Loans.Logging;
using System.Xml;
using EllieMae.Encompass.Collections;
using System.IO;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.ComponentModel;

namespace FolderMigrate_Library
{
    [Plugin]
    public class FolderMigrate_Plugin
    {
        public FolderMigrate_Plugin()
        {
            EncompassApplication.LoanOpened += EncompassApplication_LoanOpened;
        }

        private void EncompassApplication_LoanOpened(object sender, EventArgs e)
        {
            EncompassApplication.CurrentLoan.FieldChange += CurrentLoan_FieldChange; ;
        }
        private void CurrentLoan_FieldChange(object source, FieldChangeEventArgs e)
        {
            List<string> lister = new List<string>();
            List<string> listers = new List<string>();
            List<string> deleters = new List<string>();
            string input = e.NewValue.ToString();
            char[] delim = {';'};
            char[] secdelim = {','};
            string[] work = input.Split(delim);
            string[] newwork;
            int count = 0, trace = 0, delcount = 0;
            foreach (string s in work)
            {
                newwork = s.Split(secdelim);
                if (count > 0) { 
                    if (newwork[0].ToString() == "Delete") {
                        foreach (string str in newwork) {
                            deleters.Add(newwork[delcount].TrimStart());
                            lister.Add(newwork[delcount].TrimStart());
                            trace = listers.Count;
                            delcount++;
                        }
                    }
                    string[] secwork = s.Split(secdelim);
                    if (newwork[0].ToString() == "Append") {
                        foreach (string ss in secwork)
                        {
                            listers.Add(ss.TrimStart());
                            lister.Add(ss.TrimStart());
                        }
                    }
                }
                count++;
            }
            if (e.FieldID == "CX.SERVICES" && work[0] == "FolderMigrate_Plugin")
            {
                Encompass_Migrate.Migrate_Run(EncompassApplication.CurrentLoan, lister, listers, trace, deleters);
            }
        }
    }
    public class Encompass_Migrate: FolderMigrate_Plugin
    {
        public class Adder
        {
            public static void AddtoLog(Exception j)
            {
                string paths = @"c:\temp\Logger_Err.txt";
                if (!File.Exists(paths))
                {
                    File.WriteAllText(paths, DateTime.Now.ToString() + ": " + "\n" + j.Message);
                }
                else
                {
                    File.AppendAllText(paths, DateTime.Now.ToString() + ": " + "\n" + j.Message);
                }
            }
        }
        public static void Migrate_Run(Loan loan, List<string> lister, List<string> listers, int trace, List<string> deleters)
        {
            bool called = false;
            // Fetch a loan from the session
            //XmlDocument docs = new XmlDocument();
            try
            {
                //docs.Load(@"\\fileserver\Operations\ClientApps\Encompass-Tools\tables\FolderMigrate.xml");
                //XmlElement root = docs.DocumentElement;
                //XmlNodeList name = root.SelectNodes("/Files/File/Name");
                //XmlNodeList option = root.SelectNodes("/Files/File/Option");
                for (int i = 0; i < (listers.Count + deleters.Count); i++)
                {
                    string docname = lister[i].ToString();
                    LogEntryList search = loan.Log.TrackedDocuments.GetDocumentsByTitle(docname);
                    int count = 0;
                    List<string> list = new List<string>();
                    List<TrackedDocument> tracklist = new List<TrackedDocument>();
                    List<Attachment> lists = new List<Attachment>();
                    string path = System.IO.Path.GetTempPath();
                    path = Path.Combine(path, "Attachment"); //creating custom paths for each document's attachment
                    foreach (TrackedDocument docus in search)
                    {
                        if (docus.ReceivedDate != null)
                        {
                            tracklist.Add(docus);
                        }
                        else loan.Log.TrackedDocuments.Remove(docus);
                    }
                    tracklist = tracklist.OrderBy(x => x.DateAdded).ToList();
                    string direc = Path.GetDirectoryName(path);
                    DirectoryInfo d = new DirectoryInfo(direc);
                    int length = tracklist.Count;
                    if (length >= 1)
                    {
                        if (listers.Count > 0 && i >= trace && trace != 0)
                        {
                            for (int r = 0; r < (length - 1); r++)
                            {
                                loan.Log.TrackedDocuments.Remove(tracklist[r]);
                            }
                        }
                        else if (listers.Count == 0 && deleters.Count != 0)
                        {
                            for (int r = 0; r < (length - 1); r++)
                            {
                                loan.Log.TrackedDocuments.Remove(tracklist[r]);
                            }
                        }
                        else if (listers.Count > 0)
                        {
                                    if (length >= 1)
                                    {
                                foreach (TrackedDocument doc in search)
                                {
                                    called = false;
                                    foreach (Attachment att in doc.GetAttachments()) //downloading attachments to temp folder
                                    {
                                        string paths = System.IO.Path.GetTempPath();
                                        paths = Path.Combine(paths, att.Title + count);
                                        string ext = Path.GetExtension(att.ToString());
                                        doc.Detach(att);
                                        paths = Path.ChangeExtension(paths, ext);
                                        list.Add(Path.GetFullPath(paths));
                                        lists.Add(att);
                                        att.SaveToDisk(paths); // either find a way to delete the customDoc in cases where one exists prior Or figure a way to use customDoc when not needed
                                        called = true;
                                        count++;
                                    }
                                    //if (called == false)
                                    //{
                                    //    Attachment atts = loan.Attachments.Add(path1);
                                    //    doc.Attach(atts);
                                    //}
                                }
                        }
                            count = (list.Count - 1);
                            TrackedDocument logs = (TrackedDocument)tracklist[0];
                            foreach (var files in Directory.GetFiles(direc))
                            {
                                if (count < 0)
                                {
                                    break;
                                }
                                Attachment att = lists[count];
                                logs.Attach(att);
                                count--;
                            }
                            for (int s = 1; s < length; s++)
                            {
                                try
                                {
                                    loan.Log.TrackedDocuments.Remove(tracklist[s]);
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            catch (Exception e)
            {
                Adder.AddtoLog(e);
            }
        }
    }
}
